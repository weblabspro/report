(function($) {
	$(document).ready(function() {
		$('.print, .print a').click(function(e) {
			var addUrl = /\?([^#]+)/.exec(location.href);

			if(addUrl) {
				var url = $(this).attr('href') + '&' + addUrl[1];
				$(this).attr('href', url);
			}

			// e.stopPropagation();
			// return false;
		});
	});
})(jQuery);