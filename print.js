(function($) {
    $(document).ready(function() {
        $('.side-menu, header').remove();
        $('.page-wrapper').css({
            'padding-left': '100px'
        });
        $('.chart-graph .average').css({
            'float': 'none',
            'margin': '0'
        });
        $('.bside').css({
            'width': '1150px',
        });
        $('.totals-data .bside').css({
            'padding-top': '50px',
            'margin-bottom': '500px'
        });
        $('.totals-data .bside:last').css({
            'margin-bottom': '0px'
        });
        $('span.datepicker-separator').css({
            'padding-bottom': '14px'
        });
        $('#container').css({
            'padding-top': '50px'
        });
    });
})(jQuery);