(function($) {

	var options = {
		action: 'download', // allowed types: `show`, `download`.
		timeout: 15000
	}

	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
 
	switch(options.action) {
		case 'show': var url = getParameterByName('rdr'); break;
		case 'download': var url = location.href + '&forceDownload'; break;
	}

	var interval = setInterval(function() {
		$.ajax({
			url: url,
			success: function() {
				clearInterval(interval);
				setTimeout(function() {
					location.href = url;
					NProgress.done();
				}, options.timeout);
			}
		})

	}, 1000);

	$(document).ready(function() {
		NProgress.start();
	});
})(jQuery);